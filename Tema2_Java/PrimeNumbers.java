public class PrimeNumbers {

    public static void main(String[] args) {
        int limit = 1000000;
        for (int numar = 2; numar <= limit; numar++) {
            if(isPrime(numar)) {
                System.out.println(numar);
            }
        }
    }

    public static boolean isPrime(int numar) {

        for (int i=2; i<numar; i++) {
            if (numar % i == 0) {
                return false;
            }
        }
        return true;

        }



    }

