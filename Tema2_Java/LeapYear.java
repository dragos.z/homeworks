import java.util.Scanner;
public class LeapYear {


    public static void main(String[] args) {
        Scanner reader = new Scanner (System.in);
        System.out.println("Enter a year: ");
        int year = reader.nextInt();


        if ((checkNumber(year) == true) && (year % 400 == 0)) {
            System.out.println("Month February in " + year + " has 29 days!");
        } else if ((checkNumber(year) == true) &&(year % 100 == 0))
            System.out.println("Month February in " + year + " has 28 days!");
        else if ((checkNumber(year) == true) && (year % 4 == 0))
            System.out.println("Month February in " + year + " has 29 days!");
        else if ((checkNumber(year) == true)){
            System.out.println("Month February in " + year + " has 28 days!");
        }
        else {
            System.out.println("You need to insert a number which is between 1900 and 2016!");
        }
    }

    public static boolean checkNumber(int year) {

        if ((year >= 1900) && (year <= 2016)) {
            return true;
        }
        return false;
    }


}




